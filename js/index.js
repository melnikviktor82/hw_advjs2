// Теоретичне питання

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// - ми отримуємо дані від користувача, з якими буде працювати наш код;
// - ми отримуємо дані від сервера, з якими буде працювати наш код;
// - коли нам треба створити власну помилку та відловити її;
// - коли нам треба прокинути та відловити несподівану нашу помилку;
// - та ін.


// Завдання

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
const validProps = ["author", "name", "price"];

class BooksList {
  constructor(booksArr, validProps) {
    this.booksArr = booksArr;
    this.validProps = validProps;
  }

  filterAndAdd() {

    const list = document.createElement("ul");
    root.append(list);

    this.booksArr.forEach((book, index) => {
      try { 
        this.validProps.forEach((prop) => {

        if (!Object.keys(book).includes(prop)) {
          throw new Error(
            `Дані неповні: немає властивості ${prop}. Індекс елементу масиву ${index}`
          );
        }
      })
        const listItem = document.createElement("li");
        
        listItem.innerHTML = Object.entries(book).map(([key, value]) => {
          if (key === "price") {value = value + " грн."};
          return ` ${key.replace(key[0], key[0].toUpperCase())}: ${value.bold()}`
        });

        list.append(listItem);

      } catch (err) {
        console.log(err.name, err.message);
      }
     
    })
  }
}

const booksList = new BooksList(books, validProps);
booksList.filterAndAdd();
